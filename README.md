# libkonsoleqml

## Description
This project is a QML port of the Konsole terminal widget.
The code is based on QMLTermWidget, which again is based on QTermWidget which is based on Konsole.

## Install

```bash
cmake -B build && cmake --build build
sudo cmake --install build
```

## Development

The goal is to have a working terminal emulator with as little code as possible, while keeping a reasonable amount of features.
If possible, code should be shared with Konsole. Currently the following files and classes are direct copies from Konsole:
- Pty
- ShellCommand
